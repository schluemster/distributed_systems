package ch.ethz.inf.vs.freipa.vs_freipa_chat.task3;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.w3c.dom.Text;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.R;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.Message;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.MessageComparator;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.MessageTypes;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.queue.PriorityQueue;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient.NotificationHandler;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient.UdpClient;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.utils.MainActivity;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.utils.MessageReceiver;

public class ChatActivity extends AppCompatActivity implements MessageReceiver {

    NotificationHandler notificationHandler;
    UdpClient udpClient = null;
    String userName;
    String uuid;
    String address;
    int port;

    int retrieveMessagesId;
    PriorityQueue<Message> messageQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Intent intent = getIntent();
        userName = intent.getExtras().getString("username");
        uuid = intent.getExtras().getString("uuid");
        address = intent.getExtras().getString("address");
        port = intent.getExtras().getInt("port");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (udpClient == null || udpClient.isHalted())
        {
            udpClient = new UdpClient();
        }
        this.notificationHandler = new NotificationHandler(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        udpClient.haltAndCleanUp();
    }

    public void retrieveChatLog(View view) {
        Message message = new Message(userName, MessageTypes.RETRIEVE_CHAT_LOG, uuid, null, "{}");
        Log.d("CHATACTIVITY", message.createJSONMessage().toString());
        retrieveMessagesId = udpClient.postMessage(message, notificationHandler, address, port);
    }

    @Override
    public void onBackPressed() {
        Message deregPacket = new Message(userName, MessageTypes.DEREGISTER, uuid, null, "{}");
        final Intent intent = new Intent(this, MainActivity.class);
        udpClient.postMessage(deregPacket, this.notificationHandler, address, port);

        new AlertDialog.Builder(this)
                .setTitle("Notification")
                .setMessage("You have been deregistered")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
        // Execute some code after 2 seconds have passed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
            }
        }, 1000);

    }

    @Override
    public void onPacketSent(int requestId) {

    }

    @Override
    public void onResultAvailable(int requestId) {
        if (requestId == retrieveMessagesId) {
            try {
                MessageComparator messageComparator = new MessageComparator();
                messageQueue = new PriorityQueue<>(messageComparator);
                String resultList = udpClient.getResult(requestId);
                String[] messages = resultList.split("-:::-");
                for (String s : messages)
                {
                    messageQueue.add(Message.parse(s));
                }
                TextView textView = (TextView)findViewById(R.id.text_chatlog);
                StringBuilder stringBuilder = new StringBuilder();
                while (!messageQueue.isEmpty())
                {
                    Message message = messageQueue.poll();
                    stringBuilder.append(message.getUsername() + ":\n");
                    stringBuilder.append(message.getBody().getString("content"));
                    stringBuilder.append("\n\n\n");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onPacketNotSent(int requestId) {

    }

    @Override
    public void onReceiveTimeOut(int requestId) {

    }

    @Override
    public void onReceiveError(int requestId) {

    }
}
