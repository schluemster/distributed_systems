package ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient;

/**
 * Created by Jonathan on 01.11.2016.
 */

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.Message;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.MessageTypes;

public class UdpClient {

    private ConcurrentLinkedQueue<SendInformation> messageQueue;
    private ConcurrentMap<Integer, String> results = new ConcurrentHashMap<>();
    private Thread executor;
    private boolean halted;
    private int lastId = 0;
    private Semaphore postSemaphore = new Semaphore(1, true);
    private Semaphore runningSemaphore = new Semaphore(1, true);

    public UdpClient() {
        messageQueue = new ConcurrentLinkedQueue<>();
        executor = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!halted) {
                    SendInformation sendInformation = messageQueue.poll();
                    if (sendInformation != null) {
                        NotificationHandler handler = sendInformation.handler;
                        int id = sendInformation.id;
                        DatagramSocket socket = null;
                        try {
                            runningSemaphore.acquire();
                            Log.d("EXECUTOR", "handling message #"+id);
                            String outMessage = sendInformation.message.createJSONMessage().toString();
                            socket = new DatagramSocket();
                            socket.setBroadcast(true);
                            DatagramPacket outPacket = new DatagramPacket(outMessage.getBytes(), outMessage.length(), InetAddress.getByName(sendInformation.address), sendInformation.port);
                            socket.send(outPacket);
                            sendNotification(handler, NotificationHandler.PACKET_SENT, id);
                            try {
                                socket.setSoTimeout(NetworkConsts.SOCKET_TIMEOUT);
                                // get response
                                int inLength = NetworkConsts.PAYLOAD_SIZE;
                                String stringResult;
                                Log.d("EXECUTOR", "messagetype: " + sendInformation.message.getMessageType());
                                if (sendInformation.message.getMessageType().equals(MessageTypes.RETRIEVE_CHAT_LOG))
                                {
                                    stringResult = "";
                                    Log.d("EXECUTOR", "gonna retrieve some messages!");
                                    for (int i=0; i<7; i++)
                                    {
                                        byte[] inBytes = new byte[inLength];
                                        DatagramPacket inPacket = new DatagramPacket(inBytes, inLength, InetAddress.getByName(sendInformation.address), sendInformation.port);
                                        socket.receive(inPacket);
                                        stringResult += new String(inPacket.getData(), 0, inPacket.getLength()) + "-:::-";
                                        Log.d("EXECUTOR", "current result: " + stringResult);
                                    }
                                } else {
                                    byte[] inBytes = new byte[inLength];
                                    DatagramPacket inPacket = new DatagramPacket(inBytes, inLength);
                                    socket.receive(inPacket);
                                    stringResult = new String(inPacket.getData(), 0, inPacket.getLength());
                                }
                                if (stringResult.isEmpty()) {
                                    sendNotification(handler, NotificationHandler.RECEIVE_TIMEOUT, id);
                                } else {
                                    results.put(id, stringResult);
                                    sendNotification(handler, NotificationHandler.RESULT_AVAILABLE, id);
                                }
                                //socket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                                sendNotification(handler, NotificationHandler.RECEIVE_ERROR, id);
                            }
                            Thread.sleep(100);
                        } catch (IOException | InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            if (socket != null) {
                                socket.close();
                            }
                            runningSemaphore.release(1);
                        }
                    }
                }
            }
        });
        this.halted = false;
        executor.start();
    }

    private void sendNotification(NotificationHandler handler, int notificationType, Integer id)
    {
        handler.sendMessage(android.os.Message.obtain(handler, notificationType, id));
    }

    public boolean haltAndCleanUp() {
        boolean result = true;
        try {
            runningSemaphore.acquire();
            this.halted = true;
            executor.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
            result = false;
        } finally {
            runningSemaphore.release();
        }
        return result;
    }

    public boolean isHalted() {
        return halted;
    }

    public String getResult(Integer messageId) throws IllegalArgumentException
    {
        String result = results.get(messageId);
        if (result == null || result.isEmpty())
        {
            throw new IllegalArgumentException("no results for message " + messageId + "!");
        } else {
            return result;
        }
    }

    public int postMessage(Message message, NotificationHandler handler, String address, int port) {
        int result;
        try {
            postSemaphore.acquire();
            lastId = ++lastId < 0 ? 0 : lastId;
            final int id = lastId;
            messageQueue.add(new SendInformation(message, handler, address, port, id));
            result = lastId;
            postSemaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
            result = -1;
        } finally {
            postSemaphore.release();
        }

        return result;
    }

    private class SendInformation {
        Message message;
        NotificationHandler handler;
        String address;
        int port;
        int id;

        SendInformation(Message message, NotificationHandler handler, String address, int port, int id) {
            this.message = message;
            this.handler = handler;
            this.address = address;
            this.port = port;
            this.id = id;
        }
    }
}

