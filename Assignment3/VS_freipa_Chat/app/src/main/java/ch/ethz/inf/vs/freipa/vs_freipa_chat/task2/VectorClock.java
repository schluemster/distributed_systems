package ch.ethz.inf.vs.freipa.vs_freipa_chat.task2;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.clock.Clock;

/**
 * Created by ary on 28/10/16.
 */

public class VectorClock implements Clock {
    private Map<Integer, Integer> vector = new HashMap<>();

    @Override
    public void update(Clock other) {
        JSONObject otherJson;
        try {
            otherJson = new JSONObject(other.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        // If the clock string is empty, there is nothing to update.
        if (otherJson.length() == 0) {
            return;
        }

        // Iterate over the other clock.
        for (int i = 0; i < otherJson.names().length(); i++) {
            int pid;
            int time;
            try {
                pid = otherJson.names().getInt(i);
                time = otherJson.getInt(otherJson.names().getString(i));
            } catch (JSONException e) {
                // Invalid clock string.
                return;
            }
            if (vector.get(pid) != null) {
                vector.put(pid, Math.max(vector.get(pid), time));
            } else {
                addProcess(pid, time);
            }
        }
    }

    @Override
    public void setClock(Clock other) {
        setClockFromString(other.toString());
    }

    @Override
    public void tick(Integer pid) {
        vector.put(pid, getTime(pid) + 1);
    }

    @Override
    public boolean happenedBefore(Clock other) {
        JSONObject otherJson;
        try {
            otherJson = new JSONObject(other.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        // Iterate over the other clock.
        for (int i = 0; i < otherJson.names().length(); i++) {
            int pid;
            int otherClockTime;
            try {
                pid = otherJson.names().getInt(i);
                otherClockTime = otherJson.getInt(otherJson.names().getString(i));
            } catch (JSONException e) {
                // Invalid clock string.
                e.printStackTrace();
                return false;
            }
            Integer currentClockTime = vector.get(pid);
            if (currentClockTime != null && otherClockTime < currentClockTime ) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return new JSONObject(vector).toString();
    }

    @Override
    public void setClockFromString(String clock) {
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(clock);
        } catch (JSONException e) {
            return;
        }

        // Save the old clock vector for later.
        Map oldVector = this.vector;
        Map<Integer, Integer> newVector = new HashMap<>();
        this.vector = newVector;

        // If the clock string is empty, there is no need to iterate over it.
        if (jsonObject.length() == 0) {
            return;
        }

        // Iterate over the clock string.
        for(int i = 0; i<jsonObject.names().length(); i++) {
            int pid = 0;
            int time = 0;
            try {
                pid = jsonObject.names().getInt(i);
                time = jsonObject.getInt(jsonObject.names().getString(i));
            } catch (JSONException e) {
                // Invalid clock string, reset to old clock.
                this.vector = oldVector;
                return;
            }
            addProcess(pid, time);
        }
    }

    public int getTime(Integer pid) {
        return vector.get(pid);
    }

    public void addProcess(Integer pid, int time) {
        vector.put(pid, time);
    }
}
