package ch.ethz.inf.vs.freipa.vs_freipa_chat.message;

import android.util.Log;

import java.util.Comparator;
import java.util.Date;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.clock.VectorClockComparator;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.Message;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.task2.VectorClock;

/**
 * Message comparator class. Use with PriorityQueue.
 */
public class MessageComparator implements Comparator<Message> {

    static VectorClockComparator comparator = new VectorClockComparator();

    @Override
    public int compare(Message lhs, Message rhs) {
        VectorClock clock1 = new VectorClock();
        String ts1[] = lhs.getTimestamp().split(":");
        for (int i=0; i<3; i++)
        {
            int time = Integer.valueOf(ts1[i+1].substring(0, 1));
            clock1.addProcess(i, time);
            Log.d("CLOCK1", "proc " + i +": " + time);
        }
        VectorClock clock2 = new VectorClock();
        String ts2[] = rhs.getTimestamp().split(":");
        for (int i=0; i<3; i++)
        {
            int time = Integer.valueOf(ts2[i+1].substring(0, 1));
            clock2.addProcess(i, time);
            Log.d("CLOCK2", "proc " + i +": " + time);
        }
        return comparator.compare(clock1, clock2);
    }

}
