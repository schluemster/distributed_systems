package ch.ethz.inf.vs.a2.freipa.webservices.task2;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HTTPFields;
import ch.ethz.inf.vs.a2.freipa.webservices.sensor.AbstractSensor;

import java.io.IOException;
import java.io.StringReader;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;
import ch.ethz.inf.vs.a2.freipa.webservices.sensor.AbstractSensor.AsyncWorker;



/**
 * Created by Jonathan on 20.10.2016.
 */

public class SoapSensor extends AbstractSensor {
    private static final String HOST = "vslab.inf.ethz.ch";
    private static final int PORT = 8080;
    private static final String PATH = "/SunSPOTWebServices/SunSPOTWebservice";
    private static final String URL = "http://"+HOST+":"+PORT+PATH;
    //private static String WSDL = "http://vslab.inf.ethz.ch:8080/SunSPOTWebServices/SunSPOTWebservice?wsdl";
    private static final String METHOD_URL = "http://webservices.vslecture.vs.inf.ethz.ch/";
    private static final String METHOD_NAME = "getSpot";
    public static final double errorTemperature = -1000;



    @Override
    public String executeRequest() {
        SoapObject request = new SoapObject(METHOD_URL, METHOD_NAME);
        request.addProperty("id","Spot3");

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        envelope.implicitTypes = true;
        envelope.setAddAdornments(false);
        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);

        try {
            //httpTransportSE.debug = true;
            httpTransportSE.call(URL, envelope);
            //return httpTransportSE.responseDump;
            SoapObject response = (SoapObject)envelope.getResponse();
            //Log.d("response", String.valueOf(response));

            for(int i = 0; i < response.getPropertyCount(); ++i){
                PropertyInfo pi = new PropertyInfo();
                response.getPropertyInfo(i, pi);
                if(pi.name.equals("temperature")){
                    return response.getProperty(i).toString();
                }
            }

            return null;

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public double parseResponse(String response) {
        if(response != null)
            return Double.parseDouble(response);
        else
            return errorTemperature;
    }
}
