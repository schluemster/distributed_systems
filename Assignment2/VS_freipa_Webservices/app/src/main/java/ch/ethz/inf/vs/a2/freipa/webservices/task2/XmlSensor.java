package ch.ethz.inf.vs.a2.freipa.webservices.task2;

import android.icu.util.Output;
import android.os.AsyncTask;
import android.util.Log;


import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpMethods;
import ch.ethz.inf.vs.a2.freipa.webservices.sensor.AbstractSensor.AsyncWorker;

import java.io.IOException;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.sensor.AbstractSensor;

import static java.net.Proxy.Type.HTTP;

/**
 * Created by Jonathan on 20.10.2016.
 */

public class XmlSensor extends AbstractSensor {
    private static final String HOST = "vslab.inf.ethz.ch";
    private static final int PORT = 8080;
    private static String PATH = "/SunSPOTWebServices/SunSPOTWebservice";
    String SOAPs = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<S:Header/>\n<S:Body>\n<ns2:getSpot xmlns:ns2=\"http://webservices.vslecture.vs.inf.ethz.ch/\">\n<id>Spot3</id>\n</ns2:getSpot>\n</S:Body>\n</S:Envelope>";
    private static final String URL = "http://"+HOST+":"+PORT+PATH;
    @Override
    public String executeRequest() throws Exception {

        URL url = new URL(URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(HttpMethods.POST);
        connection.setRequestProperty("Content-type", "text/xml");
        connection.setRequestProperty("SOAPAction","MonitoringService");

        OutputStream reqStream = connection.getOutputStream();
        reqStream.write(SOAPs.getBytes());

        InputStream resStream = connection.getInputStream();
        byte[] byteBuf = new byte[10240];
        int len = resStream.read(byteBuf);


        Log.d("Response", connection.getResponseMessage());
        Log.d("Message", "");
        return resStream.toString();
    }



    @Override
    public double parseResponse(String response) {
        XmlPullParser xmlPullParser = null;
        String temperature = "NaN";

        try {
            xmlPullParser = XmlPullParserFactory.newInstance().newPullParser();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        StringReader sr = new StringReader(response);
        try {
            xmlPullParser.setInput(sr);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        try {
            while(xmlPullParser.getEventType()!=XmlPullParser.END_DOCUMENT)
            {
                if((xmlPullParser.getEventType()==XmlPullParser.START_TAG)&&xmlPullParser.getName().equals("temperature"))
                {
                    xmlPullParser.next();
                    temperature=xmlPullParser.getText();
                    break;

                }
                xmlPullParser.next();
            }
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return Double.parseDouble(temperature);
    }


}
