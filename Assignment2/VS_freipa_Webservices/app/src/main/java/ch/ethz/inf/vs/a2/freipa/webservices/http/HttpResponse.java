package ch.ethz.inf.vs.a2.freipa.webservices.http;

import java.io.InputStream;

/**
 * Created by schluemi on 20/10/16.
 */

public class HttpResponse extends HttpPacket {
    String status = "";
    String reason = "";

    public HttpResponse()
    {
        super();
    }

    public HttpResponse(InputStream inputstream)
    {
        parse(inputstream);
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getReason()
    {
        return reason;
    }

    @Override
    protected String generateFirstHeaderLine() {
        return version + " " + status.toString() + " " + reason + newline;
    }

    @Override
    protected void parseFirstHeaderLine(String line)
    {
        String[] requestLine = line.split(" ");
        version = requestLine[0];
        status = requestLine[1];
        reason = requestLine[2];
    }
}
