package ch.ethz.inf.vs.a2.freipa.webservices.task1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import ch.ethz.inf.vs.a2.freipa.webservices.R;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRawRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.sensor.SensorListener;

/**
 * Created by ary on 23/10/16.
 */

public class Task1MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task1main);
    }

    public void buttonRawClick(View view) {
        Intent intent = new Intent(this, RawListener.class);
        startActivity(intent);
    }

    public void buttonTextClick(View view) {
        Intent intent = new Intent(this, TextListener.class);
        startActivity(intent);
    }

    public void buttonJsonClick(View view) {
        Intent intent = new Intent(this, JsonListener.class);
        startActivity(intent);
    }

    static public class RawListener extends AppCompatActivity implements SensorListener {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sensorlistener);
        }

        @Override
        protected void onResume() {
            super.onResume();
            RawHttpSensor httpSensor = new RawHttpSensor();
            httpSensor.registerListener(this);
            httpSensor.getTemperature();
        }

        @Override
        public void onReceiveSensorValue(double value) {
            TextView text = (TextView) findViewById(R.id.text_sensorlistener);
            text.setText(getResources().getString(R.string.text_temperature) + " " + String.valueOf(value));
        }

        @Override
        public void onReceiveMessage(String message) {
            Log.d("SensorListenerError", message);
        }
    }

    static public class TextListener extends AppCompatActivity implements SensorListener {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sensorlistener);
        }

        @Override
        protected void onResume() {
            super.onResume();
            TextSensor textSensor = new TextSensor();
            textSensor.registerListener(this);
            textSensor.getTemperature();
        }

        @Override
        public void onReceiveSensorValue(double value) {
            TextView text = (TextView) findViewById(R.id.text_sensorlistener);
            text.setText(getResources().getString(R.string.text_temperature) + " " + String.valueOf(value));
        }

        @Override
        public void onReceiveMessage(String message) {
            Log.d("SensorListenerError", message);
        }
    }

    static public class JsonListener extends AppCompatActivity implements SensorListener {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sensorlistener);
        }

        @Override
        protected void onResume() {
            super.onResume();
            JsonSensor jsonSensor = new JsonSensor();
            jsonSensor.registerListener(this);
            jsonSensor.getTemperature();
        }

        @Override
        public void onReceiveSensorValue(double value) {
            TextView text = (TextView) findViewById(R.id.text_sensorlistener);
            text.setText(getResources().getString(R.string.text_temperature) + " " + String.valueOf(value));
        }

        @Override
        public void onReceiveMessage(String message) {
            Log.d("SensorListenerError", message);
        }
    }
}
