package ch.ethz.inf.vs.a2.freipa.webservices.task1;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpResponse;
import ch.ethz.inf.vs.a2.freipa.webservices.sensor.AbstractSensor;

/**
 * Created by ary on 21/10/16.
 */

public class RawHttpSensor extends AbstractSensor {
    @Override
    public String executeRequest() throws Exception {
        String host = "vslab.inf.ethz.ch";
        int port = 8081;
        String path = "/sunspots/Spot1/sensors/temperature";

        HttpRawRequestImpl request = new HttpRawRequestImpl();
        String rawRequest = request.generateRequest(host, port, path);

        Socket socket = new Socket(host, port);
        PrintWriter printWriter = new  PrintWriter(socket.getOutputStream());
        printWriter.println(rawRequest);
        printWriter.flush();


        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;
        StringBuilder str = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            str.append(line);
        }
        bufferedReader.close();
        return str.toString();


        // Doesn't work currently
        // HttpResponse response = new HttpResponse(socket.getInputStream());
        // return response.getBody();
    }

    @Override
    public double parseResponse(String response) {
        Log.d("RawHttpSensor", response);
        String temp = response.substring(response.lastIndexOf("e") + 1);
        return Double.parseDouble(temp);
    }
}
