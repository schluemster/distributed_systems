package ch.ethz.inf.vs.a2.freipa.webservices.task3;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import ch.ethz.inf.vs.a2.freipa.webservices.R;

/**
 * Created by schluemi on 20/10/16.
 */

public class RESTServerActivity extends AppCompatActivity implements ViewUpdateCallback {
    NetworkInterface iface = null;
    RESTService restService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restserver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements())
            {
                NetworkInterface networkInterface = interfaces.nextElement();
                String name = networkInterface.getName();
                Log.d("RESTACTIVITY", "found interface " + name);
                if (name.equals("wlan0"))
                {
                    iface = networkInterface;
                    break;
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        TextView ipField = (TextView) findViewById(R.id.text_ip);
        if (iface == null)
        {
            ipField.setText("ERROR: make sure wifi device is called wlan0");
        } else {

            Log.d("RESTACTIVITY", "going to bind that service...");
            Intent intent = new Intent(this, RESTService.class);
            InetAddress ip4addr = null;
            Enumeration<InetAddress> addresses = iface.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress address = addresses.nextElement();
                if (address instanceof Inet4Address) {
                    ip4addr = address;
                    break;
                }
            }

            if (ip4addr != null) {
                intent.putExtra(getString(R.string.parameter_iface), ip4addr);
                bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
            } else {
                ipField.setText("ERROR: make sure your wlan0 device is connected to a hotspot and has a valid IPv4 address!");
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (restService != null) {
            restService.stop();
            unbindService(serviceConnection);
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("RESTACTIVITY", "the service is connected");
            RESTService.RestServiceBinder binder = (RESTService.RestServiceBinder) service;
            restService = binder.getService();
            restService.setViewUpdateCallback(RESTServerActivity.this);
            restService.openSocket();
            restService.run();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            restService = null;
        }
    };

    @Override
    public void updateTextView(final int id, final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView)findViewById(id)).setText(text);
            }
        });
    }

    @Override
    public void setEnabled(int id, boolean enabled) {
        findViewById(id).setEnabled(enabled);
    }
}
