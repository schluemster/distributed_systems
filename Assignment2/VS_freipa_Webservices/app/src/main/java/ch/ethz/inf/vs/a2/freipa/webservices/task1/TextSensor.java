package ch.ethz.inf.vs.a2.freipa.webservices.task1;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ch.ethz.inf.vs.a2.freipa.webservices.sensor.AbstractSensor;

/**
 * Created by ary on 22/10/16.
 */

public class TextSensor extends AbstractSensor {
    @Override
    public String executeRequest() throws Exception {
        String urlString = "http://vslab.inf.ethz.ch:8081/sunspots/Spot1/sensors/temperature";

        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "text/plain");
        connection.setRequestProperty("Connection", "close");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        StringBuilder str = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            str.append(line);
        }
        bufferedReader.close();
        return str.toString();
    }

    @Override
    public double parseResponse(String response) {
        Log.d("TextSensor", response);
        return Double.parseDouble(response);
    }
}
