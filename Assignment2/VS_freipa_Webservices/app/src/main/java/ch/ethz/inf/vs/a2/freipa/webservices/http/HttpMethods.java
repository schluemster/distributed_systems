package ch.ethz.inf.vs.a2.freipa.webservices.http;

/**
 * Created by schluemi on 20/10/16.
 */

public abstract class HttpMethods {
    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
}
