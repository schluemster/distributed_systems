package ch.ethz.inf.vs.a2.freipa.webservices.task3.pages;


import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpResponse;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.HtmlBuilder;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.SensorTypes;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.SensorTypesImpl;
import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;
import ch.ethz.inf.vs.a2.freipa.webservices.utils.Tuple;

/**
 * Created by schluemi on 24/10/16.
 */

public class SensorGet extends WebPage {
    public SensorGet(String name, String link, StringMap navi, HttpRequest request, HttpResponse response) {
        super(name, link, navi, request, response);
    }

    @Override
    public String generateHtml() {

        String result;
        try {
            Integer type = Integer.valueOf(request.getGetVariables().get("type"));
            Tuple<String, float[]> t = (Tuple<String, float[]>) dataValues.get(type);

            StringMap data = new StringMap();
            int i = 1;
            String sensorName = t.first;
            float[] values = t.second;
            this.name = sensorName;
            SensorTypes sensorTypes = new SensorTypesImpl();
            for (float f : values) {
                data.put("Value " + i + ": ", String.valueOf(f) + " " + sensorTypes.getUnitString(type));
                i++;
            }
            result = HtmlBuilder.SimplePage(name, data, navi);
        } catch (NumberFormatException e){
            e.printStackTrace();
            result = HtmlBuilder.SimplePage(name, "There is currently no data available for the requested sensor. Please come back later!", navi);
        }

        return result;
    }
}
