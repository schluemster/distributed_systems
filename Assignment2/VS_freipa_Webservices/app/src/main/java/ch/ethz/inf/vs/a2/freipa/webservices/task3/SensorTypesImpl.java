package ch.ethz.inf.vs.a2.freipa.webservices.task3;

/**
 * Created by schluemi on 23/10/16.
 */

public class SensorTypesImpl implements SensorTypes {
    public int getNumberValues(int sensorType){
        switch(sensorType){
            case 1: return 3;
            case 13: return 1;
            case 9: return 3;
            case 4: return 3;
            case 5: return 1;
            case 10: return 3;
            case 2: return 3;
            case 3: return 3;
            case 6: return 1;
            case 8: return 1;
            case 12: return 1;
            case 7: return 1;
            case 11: return 3;
            default: return -1;
        }

    }

    public String getUnitString(int sensorType){
        switch(sensorType){
            case 1: return "m/s^2";
            case 13: return "°C";
            case 9: return "m/s^2";
            case 4: return "rad/s";
            case 5: return "lx";
            case 10: return "m/s^2";
            case 2: return "microT";
            case 3: return "deg";
            case 11: return "no unit";
            case 6: return "hPa";
            case 8: return "cm";
            case 12: return "%";
            case 7: return "celsius";
            default: return "no unit";
        }

    }
}