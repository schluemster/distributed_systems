package ch.ethz.inf.vs.a2.freipa.webservices.task3.pages;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpResponse;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.HtmlBuilder;
import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;

/**
 * Created by schluemi on 23/10/16.
 */

public class MainPage extends WebPage {
    public MainPage(String name, String link, StringMap navi, HttpRequest request, HttpResponse response) {
        super(name, link, navi, request, response);
    }

    @Override
    public String generateHtml() {
        return HtmlBuilder.SimplePage(name, "Please choose a sensor or actuator in the navigation above!", navi);
    }
}
