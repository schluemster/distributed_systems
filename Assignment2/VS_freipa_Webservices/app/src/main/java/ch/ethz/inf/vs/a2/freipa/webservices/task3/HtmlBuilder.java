package ch.ethz.inf.vs.a2.freipa.webservices.task3;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;

/**
 * Created by schluemi on 21/10/16.
 */

public class HtmlBuilder {

    public static String SimplePage(String title, String data, StringMap links) {
        return makePage(title, data, links);
    }

    public static String SimplePage(String title, Map<? extends Object, ? extends Object> data, StringMap links) {
        String content = tableBuilder(data);
        return makePage(title, content, links);
    }

    public static String SimplePage(String title, Collection data, StringMap links) {
        String content = "<ul>";
        for (Object o : data) {
            content += "<li>" + o.toString() + "</li>";
        }
        content += "</ul>";
        return makePage(title, content, links);
    }

    public static String SimplePage(String title, SortedSet data, StringMap links) {
        String content = "<ol>";
        for (Object o : data) {
            content += "<li>" + o.toString() + "</li>";
        }
        content += "</ol>";
        return makePage(title, content, links);
    }

    public static String FormPage(String title, StringMap form, String target, String method, String description, StringMap links)
    {
        String content = "<form action='"+target+"' method='"+method+"'>";
        StringMap data = new StringMap();
        for (Map.Entry<String, String> e : form.entrySet())
        {
            String input = "<input type='text' name='" + e.getValue() + "'";
            data.put(e.getKey(), input);
        }
        data.put("", "<input type='submit' value='Send request'/>");
        content += tableBuilder(data);
        content += "</form>";
        content += "<p>" + description + "</p>";
        return makePage(title, content, links);
    }

    public static String FormPage(String title, StringMap form, String target, String method, StringMap links)
    {
        return FormPage(title, form, target, method, "", links);
    }

    public static String ErrorPage(String code, String reason, StringMap links)
    {
        return makePage(code + " " + reason, "", links);
    }

    public static String ErrorPage(String code, String reason)
    {
        String content = htmlBegin(code);
        content += "<h1>" + code + " " + reason + "<h1>";
        content += htmlEnd();
        return content;
    }

    private static String makePage(String title, String content, StringMap links)
    {
        String returner = htmlBegin(title);
        returner += "<h1>" + title + "</h1><div id='navigation'>| ";
        for (Map.Entry<String, String> link : links.entrySet()) {
            returner += "<a href='" + link.getKey() + "'>" + link.getValue() + "</a> | ";
        }
        returner += "</div><div id='content'>";
        returner += content;
        returner += "</div>";
        returner += htmlEnd();
        return returner;
    }

    private static String tableBuilder(Map<? extends Object, ? extends Object> data)
    {
        String content = "<table>";
        for (Map.Entry entry : data.entrySet())
        {
            content += "<tr><td>" + entry.getKey() + "</td><td>" + entry.getValue() + "</td></tr>";
        }
        content += "</table>";
        return content;
    }

    private static String htmlBegin(String title) {
        return "<html><head><style>"+getCss()+"</style><title>" + title + "</title></head><body><div id='main'>";
    }

    private static String htmlEnd() {
        return "</div></body></html>";
    }

    private static String getCss()
    {
        String css = "" +
                "*{" +
                "font-family: verdana, arial;" +
                "}" +

                "html, body{" +
                "min-heigth: 100%;" +
                "}"+

                "html{" +
                "min-height: 100%;"+
                "background-color: #01a1d5;" +
                "background: linear-gradient(to bottom, #01a1d5, #0085b1);" +
                "}" +

                "div#main{" +
                "display: block;" +
                "margin: 2.6em;" +
                "padding-top: 2em;" +
                "padding-bottom: 2em;" +
                "padding-left: 3.2em;" +
                "padding-right: 3.2em;" +
                "background-color: #ffffff;" +
                "box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.5);" +
                "}" +
                "" +
                "a, #navigation a {" +
                "text-decoration: none;" +
                "color: #0000ff;" +
                "}" +
                "" +
                "a:hover {" +
                "text-decoration: underline" +
                "}" +
                "" +
                "a:visited {" +
                "color: #0000ff;" +
                "}" +
                "" +
                "div#content{" +
                "padding-top: 2.5em;" +
                "min-height: 15em" +
                "font-size: 1.1em;" +
                "}" +
                "" +
                "div#navigation{" +
                "font-size: 0.9em;" +
                "color: #7d979f;" +
                "}" +
                "" +
                "h1{" +
                "padding-top: 0;" +
                "margin-top: 0;" +
                "font-size: 1.7em;" +
                "}";

        return css;
    }
}
