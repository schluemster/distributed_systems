package ch.ethz.inf.vs.a2.freipa.webservices.task1;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ch.ethz.inf.vs.a2.freipa.webservices.sensor.AbstractSensor;

/**
 * Created by ary on 23/10/16.
 */

public class JsonSensor extends AbstractSensor {
    @Override
    public String executeRequest() throws Exception {
        String urlString = "http://vslab.inf.ethz.ch:8081/sunspots/Spot1/sensors/temperature";

        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Connection", "close");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        StringBuilder str = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            str.append(line);
        }
        bufferedReader.close();
        return str.toString();
    }

    @Override
    public double parseResponse(String response) {
        Log.d("JsonSensor", response);
        JSONObject jsonObject;
        Double temperature = null;
        try {
            jsonObject = new JSONObject(response);
            temperature = jsonObject.getDouble("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return temperature;
    }
}
