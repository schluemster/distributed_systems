package ch.ethz.inf.vs.a2.freipa.webservices.http;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;

/**
 * Created by schluemi on 20/10/16.
 */

public abstract class HttpPacket {
    StringMap headerFields = new StringMap();
    String version = "";
    String body = "";
    String newline = "\r\n";
    StringMap postVariables = new StringMap();
    StringMap getVariables = new StringMap();
    boolean badPacket = false;

    protected abstract String generateFirstHeaderLine();

    protected abstract void parseFirstHeaderLine(String line);

    public HttpPacket()
    {
        version = "HTTP/1.1";
    }

    public StringMap getGetVariables()
    {
        return getVariables;
    }

    public void setGetVariables(StringMap variables)
    {
        this.getVariables = variables;
    }

    public StringMap getPostVariables()
    {
        return postVariables;
    }

    public void setPostVariables(StringMap variables)
    {
        this.postVariables = variables;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public StringMap getHeaderFields()
    {
        return headerFields;
    }

    public String getHeaderField(String name)
    {
        return headerFields.get(name);
    }

    public void setHeaderField(String name, String value)
    {
        headerFields.put(name, value);
    }

    public boolean containsHeaderField(String name)
    {
        return headerFields.containsKey(name);
    }

    public String removeHeaderField(String name)
    {
        return headerFields.remove(name);
    }

    public void setPostVariable(String name, String value)
    {
        postVariables.put(name, value);
    }

    public boolean containsPostVariable(String name)
    {
        return postVariables.containsKey(name);
    }

    public String removePostVariable(String name)
    {
        return postVariables.remove(name);
    }

    public void setGetVariable(String name, String value)
    {
        getVariables.put(name, value);
    }

    public boolean containsGetVariable(String name)
    {
        return getVariables.containsKey(name);
    }

    public String removeGetVariable(String name)
    {
        return getVariables.remove(name);
    }


    private String flattenedHeaderFields() {
        String result = "";
        for (Map.Entry<String, String> entry : headerFields.entrySet())
        {
            result += entry.getKey() + ": " + entry.getValue() + newline;
        }
        return result;
    }

    public String getRawData() {
        String returner = "";
        returner += generateFirstHeaderLine();
        headerFields.put(HTTPFields.Response.CONTENTLENGTH, String.valueOf(body.length()));
        returner += flattenedHeaderFields();
        returner += newline;
        returner += body;
        return returner;
    };
    protected void parse(InputStream inputstream) {
        int bodylength = 0;
        ParseStep step = ParseStep.RequestLine;
        StringBuilder line = new StringBuilder();
        boolean readingHeader = true;
        char c;
        int linebreakcounter = 0;

        for (int i=0; readingHeader && i<2048; i++) {
            try {
                c = (char) inputstream.read();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }

            if (c != '\n' && c != '\r') {
                line.append(c);
                linebreakcounter = 0;
            }

            if (c == '\r') {
                linebreakcounter++;
                String[] requestLine;
                switch (step) {
                    case RequestLine:
                        parseFirstHeaderLine(line.toString());
                        step = ParseStep.Headers;
                        break;

                    case Headers:
                        if (linebreakcounter > 1) {
                            step = ParseStep.Body;
                            if (headerFields.containsKey(HTTPFields.Request.CONTENT_LENGTH)) {
                                bodylength = Integer.valueOf(headerFields.get(HTTPFields.Request.CONTENT_LENGTH));
                            }
                            //Log.d("HTTPPARSER", "done with the dictionay, moving to the body with length: " + String.valueOf(bodylength));
                            readingHeader = false;
                            break;
                        } else {
                            requestLine = line.toString().split(": ");
                            //Log.d("HTTPPARSER", "got this dictionary item: " + requestLine[0] + " = " + requestLine[1]);
                            headerFields.put(requestLine[0], requestLine[1]);
                        }
                        break;
                }
                line = new StringBuilder();
            }
        }
        if (bodylength > 0) {
            byte[] rawbody = new byte[bodylength+1];
            try {
                inputstream.read(rawbody);
                String tempBody = new String(rawbody, "UTF-8");
                body = tempBody.substring(1);
            } catch (IOException e) {
                e.printStackTrace();
                badPacket = true;
            }
        } else {
            body = "";
        }
    }

    private enum ParseStep {
        RequestLine, Headers, Body;
    }
}
