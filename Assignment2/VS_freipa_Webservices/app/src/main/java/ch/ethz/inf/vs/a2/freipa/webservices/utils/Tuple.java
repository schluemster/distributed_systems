package ch.ethz.inf.vs.a2.freipa.webservices.utils;

/**
 * Created by schluemi on 24/10/16.
 */

public class Tuple<S, T> {
    public final S first;
    public final T second;

    public Tuple(S s, T t) {
        first = s;
        second = t;
    }

    @Override
    public boolean equals(Object o) {
        try {
            Tuple<S, T> other = (Tuple<S, T>) o;
            return first.equals(other.first) && second.equals(other.second);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return ((Integer) (first.hashCode() + second.hashCode())).hashCode();
    }
}