package ch.ethz.inf.vs.a2.freipa.webservices.task1;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HTTPFields;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpMethods;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRawRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;

/**
 * Created by ary on 20/10/16.
 */

public class HttpRawRequestImpl implements HttpRawRequest {
    @Override
    public String generateRequest(String host, int port, String path) {
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setMethod(HttpMethods.GET);
        httpRequest.setRequestUri(path);

        httpRequest.setHeaderField(HTTPFields.Request.HOST, host + ":" + port);
        httpRequest.setHeaderField(HTTPFields.Request.CONNECTION, "close");
        httpRequest.setHeaderField(HTTPFields.Request.ACCEPT, "text/plain");

        return httpRequest.getRawData();
    }
}
