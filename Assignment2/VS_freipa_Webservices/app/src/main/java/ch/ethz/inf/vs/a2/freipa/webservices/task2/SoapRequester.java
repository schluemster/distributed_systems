package ch.ethz.inf.vs.a2.freipa.webservices.task2;


/**
 * Created by Jonathan on 24.10.2016.
 */

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;
import android.util.Log;

import java.io.IOException;


public class SoapRequester {
    private HttpTransportSE httpTransportSE;
    private SoapSerializationEnvelope envelope;
    private String SOAP_ACTION;

    SoapRequester(HttpTransportSE htse, SoapSerializationEnvelope e, String sa){
        httpTransportSE = htse;
        envelope = e;
        SOAP_ACTION = sa;
    }

    public String executeRequest() throws NullPointerException{
        try {
            //httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION, envelope);
            //return httpTransportSE.responseDump;
            SoapObject response = (SoapObject)envelope.getResponse();
            //Log.d("response", String.valueOf(response));

            for(int i = 0; i < response.getPropertyCount(); ++i){
                PropertyInfo pi = new PropertyInfo();
                response.getPropertyInfo(i, pi);
                if(pi.name.equals("temperature")){
                    return response.getProperty(i).toString();
                }
            }

            return null;

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
        return null;
    }

}
