package ch.ethz.inf.vs.a2.freipa.webservices.http;

import android.util.Log;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;

/**
 * Created by schluemi on 20/10/16.
 */

public class HttpRequest extends HttpPacket {
    String requestUri;
    String requestUrl;
    String httpMethod;

    public HttpRequest() {
        super();
    }

    public HttpRequest(InputStream inputstream) {
        parse(inputstream);
        try {
                parseGetVariables(); //abusing getVariables as general url parameter container
            if (httpMethod.equals(HttpMethods.POST)) {
                parsePostVariables();
            }
        } catch (Exception e)
        {
            Log.d("HTTPPARSER", "got neither a GET nor a POST request. Probably just noise...");
        }
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String url)
    {
        requestUrl = url;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String uri) {
        this.requestUri = uri;
    }

    public String getMethod() {
        return httpMethod;
    }

    public void setMethod(String method) {
        this.httpMethod = method;
    }

    private void parseGetVariables() {
        String[] splittedUri = requestUri.split("\\?");
        requestUrl = splittedUri[0];
        try {
            String getVars = splittedUri[1];
            String kvps[] = getVars.split("\\&");
            parseVariables(getVariables, kvps);
        } catch (Exception e) {

        }
    }

    private void parsePostVariables() {
        requestUrl = requestUri;
        String kvps[] = body.split("\\&");
        parseVariables(postVariables, kvps);
    }

    private void parseVariables(StringMap target, String[] variables) {
        Log.d("HTTPPARSER", "gonna add " + variables.length + " " + httpMethod + " variables!");
        for (String s : variables) {
            String[] kvp = s.split("=");
            if (kvp.length == 2) {
                try {
                    target.put(URLDecoder.decode(kvp[0], "UTF-8"), URLDecoder.decode(kvp[1], "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    Log.d("HTTPPARSER", "could not decode variable!");
                }
            }
        }
    }

    @Override
    protected String generateFirstHeaderLine() {
        return httpMethod + " " + requestUri + " " + version + newline;
    }

    @Override
    protected void parseFirstHeaderLine(String line) {
        String[] requestLine = line.split(" ");
        httpMethod = requestLine[0];
        requestUri = requestLine[1];
        version = requestLine[2];
    }
}
