package ch.ethz.inf.vs.a2.freipa.webservices.task3.pages;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpMethods;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpResponse;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.HtmlBuilder;
import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;

/**
 * Created by schluemi on 24/10/16.
 */

public class ActuatorPage extends WebPage {

    public ActuatorPage(String name, String link, StringMap navi, HttpRequest request, HttpResponse response) {
        super(name, link, navi, request, response);
    }

    @Override
    public String generateHtml() throws Exception {
        String result = "invalid actuator";

        String actuator = request.getGetVariables().get("actuator");
        String method = request.getMethod();

        if (actuator.equals("vibrator")) {
            if (method.equals(HttpMethods.GET)) {
                result = vibratorGet();
            } else if (method.equals(HttpMethods.POST)) {
                result = vibratorPost();
            }
        } else if (actuator.equals("music")) {
            if (method.equals(HttpMethods.GET)) {
                result = musicGet();
            } else if (method.equals(HttpMethods.POST)) {
                result = musicPost();
            }
        }

        return result;
    }

    private String vibratorGet() {
        StringMap patternForm = new StringMap();
        patternForm.put("Vibration pattern", "pattern");
        return HtmlBuilder.FormPage(name, patternForm, request.getRequestUri(), HttpMethods.POST, "Please enter a comma separated integer (0 <= x <= 20000) sequence (vib,pause,vib,pause,...) in milliseconds and submit it to the server!", navi);
    }

    private String vibratorPost() {
        String result = "";
        try {
            String[] values = request.getPostVariables().get("pattern").split(",");
            List<Integer> intvalues = new ArrayList<>();
            Log.d("ACTUATORPAGE", "#values: " + values.length);
            for (String s : values) {
                Integer i = Integer.valueOf(s);
                if (0<=i && i<=20000) {
                    intvalues.add(i);
                } else {
                    throw new Exception();
                }
            }
            if (actuatorService.vibrate(intvalues)) {
                result = HtmlBuilder.SimplePage(name, "The phone is vibrating now!", navi);
            } else {
                result = HtmlBuilder.SimplePage(name, "Another vibration pattern is in progress. Please come back later!", navi);
                response.setStatus("202");
                response.setReason("Accepted");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus("400");
            response.setReason("Bad request, please follow the instructions or don't use my tool!");
            result = HtmlBuilder.ErrorPage(response.getStatus(), response.getReason(), navi);
        }
        return result;
    }

    private String musicGet() {
        return HtmlBuilder.FormPage(name, new StringMap(), request.getRequestUri(), HttpMethods.POST, "Just press the button and whoever carries the phone will get annoyed!", navi);
    }

    private String musicPost() {
        String result ="";
        if (actuatorService.playMusic()) {
            result = HtmlBuilder.SimplePage(name, "The phone is playing an annoying sound now!", navi);
        } else {
            result = HtmlBuilder.SimplePage(name, "Someone else is already playing the annoying sound. Please come back later", navi);
            response.setStatus("202");
            response.setReason("Accepted");
        }
        return result;
    }
}