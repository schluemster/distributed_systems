package ch.ethz.inf.vs.a2.freipa.webservices.task3;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;

/**
 * Created by schluemi on 23/10/16.
 */

public class SensorService extends Service implements SensorEventListener {
    private SensorManager sensorManager;
    IBinder iBinder = new SensorServiceBinder();
    SensorTypes sensorTypes = new SensorTypesImpl();

    @Override
    public void onCreate()
    {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        super.onCreate();
        List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        for (Sensor sensor : sensorList) {
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        sensorManager.unregisterListener(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        //Log.d("SENSORSERVICE", "help, someone's binding meee :O");
        return iBinder;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        Intent intent = new Intent("");
        intent.putExtra("SensorType", event.sensor.getType());
        intent.putExtra("SensorValues", values);
        intent.putExtra("SensorName", event.sensor.getName());
        sendBroadcast(intent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //let's not do anything
    }

    public class SensorServiceBinder extends Binder {
        SensorService getService() {
            return SensorService.this;
        }
    }
}
