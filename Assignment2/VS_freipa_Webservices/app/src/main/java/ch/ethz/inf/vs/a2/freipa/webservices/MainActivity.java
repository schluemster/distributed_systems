package ch.ethz.inf.vs.a2.freipa.webservices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import ch.ethz.inf.vs.a2.freipa.webservices.task1.Task1MainActivity;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.RESTServerActivity;
import ch.ethz.inf.vs.a2.freipa.webservices.task2.WSActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonTask1onClick(View view) {
        Intent intent = new Intent(this, Task1MainActivity.class);
        startActivity(intent);
    }

    public void buttonTask2onClick(View view) {
        Intent intent = new Intent(this, WSActivity.class);
        startActivity(intent);
    }

    public void buttonTask3onClick(View view) {
        Intent intent = new Intent(this, RESTServerActivity.class);
        startActivity(intent);
    }
}
