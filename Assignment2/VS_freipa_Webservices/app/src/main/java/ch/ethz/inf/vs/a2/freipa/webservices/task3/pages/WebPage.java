package ch.ethz.inf.vs.a2.freipa.webservices.task3.pages;

import android.app.Activity;

import java.util.Map;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpResponse;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.ActuatorService;
import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;

/**
 * Created by schluemi on 22/10/16.
 */

public abstract class WebPage {
    protected String name;
    protected String link;
    protected StringMap navi;
    protected HttpRequest request;
    protected HttpResponse response;
    protected Map dataValues;
    protected ActuatorService actuatorService;

    public WebPage()
    {
        this("", "", new StringMap(), new HttpRequest(), new HttpResponse());
    }

    public WebPage(String name, String link, StringMap navi, HttpRequest request, HttpResponse response) {
        this.name = name;
        this.link = link;
        this.navi = navi;
        this.request = request;
        this.response = response;
    }

    public void setActuatorService(ActuatorService service){
        this.actuatorService = service;
    }

    public ActuatorService getActuatorService() {
        return actuatorService;
    }

    public Map getDataValues()
    {
        return dataValues;
    }

    public void setDataValue(Map dataValues)
    {
        this.dataValues = dataValues;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public void setResponse(HttpResponse response) {
        this.response = response;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public void setRequest(HttpRequest request) {
        this.request = request;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public abstract String generateHtml() throws Exception;
}
