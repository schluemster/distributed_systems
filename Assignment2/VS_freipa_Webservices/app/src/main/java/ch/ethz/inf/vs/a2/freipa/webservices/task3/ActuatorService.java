package ch.ethz.inf.vs.a2.freipa.webservices.task3;


import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;

import java.util.List;
import java.util.concurrent.Semaphore;

import ch.ethz.inf.vs.a2.freipa.webservices.R;

/**
 * Created by schluemi on 24/10/16.
 */

public class ActuatorService extends Service {
    IBinder iBinder = new ActuatorServiceBinder();
    Vibrator vibrator;
    private MediaPlayer mp;
    Semaphore vibraSemaphore = new Semaphore(1);

    @Override
    public void onCreate()
    {
        mp = MediaPlayer.create(this, R.raw.alarm_sound);
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        super.onCreate();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

    }

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    public boolean playMusic()
    {
        if(!mp.isPlaying()) {
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                    mp = MediaPlayer.create(this, R.raw.alarm_sound);
                }
                mp.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean vibrate(final List<Integer> pattern) throws InterruptedException {
        if (!vibrator.hasVibrator())
        {
            return false;
        }

        boolean gotAccess = vibraSemaphore.tryAcquire(1);
        if (gotAccess){
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < pattern.size(); i++) {
                        if (i % 2 == 0) {
                            vibrator.vibrate(pattern.get(i));
                        }
                        try {
                            Thread.sleep(pattern.get(i));
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("ACTUATORSERVICE", "Vibrator thread was interrupted!");
                        }

                    }
                    vibraSemaphore.release(1);
                }
            });
            t.start();
        }
        return gotAccess;
    }

    public class ActuatorServiceBinder extends Binder {
        ActuatorService getService() {
            return ActuatorService.this;
        }
    }
}
