package ch.ethz.inf.vs.a2.freipa.webservices.task2;

/**
 * Created by Jonathan on 22.10.2016.
 */
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import ch.ethz.inf.vs.a2.freipa.webservices.R;
import ch.ethz.inf.vs.a2.freipa.webservices.sensor.SensorListener;

public class WSActivity extends Activity implements SensorListener{
    XmlSensor xmlSensor;
    SoapSensor soapSensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ws);

        xmlSensor = new XmlSensor();
        xmlSensor.registerListener(this);
        xmlSensor.getTemperature();

        soapSensor = new SoapSensor();
        soapSensor.registerListener(new SensorListener() {
            @Override
            public void onReceiveSensorValue(double value) {
                TextView text = (TextView) findViewById(R.id.temperatureDisplay2);
                if(value>SoapSensor.errorTemperature)
                    text.setText(value + "°C");
                else
                    text.setText("check your connection");

            }

            @Override
            public void onReceiveMessage(String message) {
                //never called
            }


        });
        soapSensor.getTemperature();

        Log.d("response", "started");
    }

    @Override
    public void onReceiveSensorValue(double value) {
        TextView text = (TextView) findViewById(R.id.temperatureDisplay);
        if(value>-1000)
            text.setText(value + "°C");
        else
            text.setText("check your connection");
    }

    @Override
    public void onReceiveMessage(String message) {

    }

    public void updateOnClick(View v){
        soapSensor.getTemperature();
        xmlSensor.getTemperature();
    }
}
