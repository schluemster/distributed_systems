package ch.ethz.inf.vs.a2.freipa.webservices.task3;

import android.view.View;

/**
 * Created by schluemi on 23/10/16.
 */

public interface ViewUpdateCallback {
    void updateTextView(int id, String text);
    void setEnabled(int id, boolean enabled);
}
