package ch.ethz.inf.vs.a2.freipa.webservices.task3;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.R;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpResponse;

/**
 * Created by schluemi on 20/10/16.
 */

public class RESTService extends Service {
    IBinder iBinder = new RestServiceBinder();
    ServerSocket serverSocket = null;
    InetAddress address;
    boolean running = false;
    Thread thread;
    private ViewUpdateCallback activity;
    private SensorService sensorService;
    private ActuatorService actuatorService = null;
    RequestHandler requestHandler;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        address = (InetAddress)intent.getExtras().getSerializable(getString(R.string.parameter_iface));
        return iBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        try {
            super.onUnbind(intent);
            sensorService.unregisterReceiver(requestHandler);
            unbindService(sensorServiceConnector);
            unbindService(actuatorServiceConnector);
            serverSocket.close();
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
        catch (NullPointerException e)
        {
            return true;
        }
    }

    public void run() {
        Intent intent = new Intent(this, SensorService.class);
        bindService(intent, sensorServiceConnector, Context.BIND_AUTO_CREATE);
    }

    private void startRequestHandling()
    {
        requestHandler = new RequestHandler();
        IntentFilter intentFilter = new IntentFilter("");
        sensorService.registerReceiver(requestHandler, intentFilter);
        Log.d("RESTSERVICE", "Actuator service exists: " + (actuatorService != null));
        requestHandler.setActuatorService(actuatorService);
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (running) {
                    try {
                        openSocket();
                        Log.d("RESTSERVICE", "waiting for a new client");
                        final Socket socket = serverSocket.accept();
                        Thread respondClientT = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                HttpRequest httpRequest;
                                try {
                                    httpRequest = new HttpRequest(socket.getInputStream());
                                    Log.d("RESTSERVICE", "received this request: " + httpRequest.getRawData());
                                    Log.d("RESTSERVICE:", "GET variables: " + httpRequest.getGetVariables().toString());
                                    Log.d("RESTSERVICE:", "POST variables: " + httpRequest.getPostVariables().toString());
                                    HttpResponse response = requestHandler.handleRequest(httpRequest);
                                    socket.getOutputStream().write(response.getRawData().getBytes());
                                    socket.close();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        respondClientT.start();
                    }
                    catch (IOException e) {
                        Log.d("RESTSERVICE", "socket might be closed, wtf?!");
                    }
                }
            }
        });
        thread.start();
    }

    public void openSocket()
    {
        if (serverSocket == null || serverSocket.isClosed()) {
            try {
                Log.d("RESTSERVICE", "gonna open a new socket");
                serverSocket = new ServerSocket(44444, 20, address);
                running = true;
                Log.d("RESTSERVICE", "new socket runs on " + serverSocket.getInetAddress().toString() + ":" + serverSocket.getLocalPort());
            } catch (IOException e) {
                try {
                    serverSocket = new ServerSocket(0, 20, address);
                    running = true;
                    Log.d("RESTSERVICE", "new socket runs on " + serverSocket.getLocalSocketAddress().toString() + ":" + serverSocket.getLocalPort());
                } catch (IOException f) {
                    e.printStackTrace();
                    running = false;
                }
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                running = false;
            }
        }

        activity.updateTextView(R.id.text_ip, getIP());
        activity.updateTextView(R.id.text_port, String.valueOf(getPort()));
    }

    public void stop()
    {
        running = false;
    }

    public void setViewUpdateCallback(ViewUpdateCallback activity)
    {
        this.activity = activity;
    }

    public int getPort()
    {
        return serverSocket.getLocalPort();
    }

    public String getIP()
    {
        return serverSocket.getInetAddress().toString();
    }

    private void startActuatorService()
    {
        Intent intent = new Intent(this, ActuatorService.class);
        bindService(intent, actuatorServiceConnector, Context.BIND_AUTO_CREATE);
    }

    public class RestServiceBinder extends Binder {
        RESTService getService() {
            return RESTService.this;
        }
    }

    private ServiceConnection sensorServiceConnector = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("RESTSERVICE", "Connected to the sensor service!");
            SensorService.SensorServiceBinder binder =  (SensorService.SensorServiceBinder) service;
            sensorService = binder.getService();
            startActuatorService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

        }
    };

    private ServiceConnection actuatorServiceConnector = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("RESTSERVICE", "Connected to the actuator service!");
            ActuatorService.ActuatorServiceBinder binder =  (ActuatorService.ActuatorServiceBinder) service;
            actuatorService = binder.getService();
            startRequestHandling();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

        }
    };
}
