package ch.ethz.inf.vs.a2.freipa.webservices.http;

/**
 * Created by schluemi on 20/10/16.
 */

public final class HTTPFields {
    public static class Request {
        public static final String ACCEPT = "Accept";
        public static final String ACCEPT_CHARSET = "Accept-Charset";
        public static final String ACCEPTENCODING = "Accept-Encoding";
        public static final String ACCEPTLANGUAGE = "Accept-Language";
        public static final String AUTHORIZATION = "Authorization";
        public static final String CACHE_CONTROL = "Cache-Control";
        public static final String CONNECTION = "Connection";
        public static final String COOKIE = "Cookie";
        public static final String CONTENT_LENGTH = "Content-Length";
        public static final String CONTENT_MD5 = "Content-MD5";
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String DATE = "Date";
        public static final String EXPECT = "Expect";
        public static final String FROM = "From";
        public static final String HOST = "Host";
        public static final String IFMATCH = "If-Match";
        public static final String IFMODIFIEDSINCE = "If-Modified-Since";
        public static final String IFNONEMATCH = "If-None-Match";
        public static final String IFRANGE = "If-Range";
        public static final String IFUNMODIFIEDSINCE = "If-Unmodified-Since";
        public static final String MAXFORWARDS = "Max-Forwards";
        public static final String PRAGMA = "Pragma";
        public static final String PROXYAUTHORIZATION = "Proxy-Authorization";
        public static final String RANGE = "Range";
        public static final String REFEREER = "Referer";
        public static final String TRANSFERENCODING = "Transfer-Encoding";
        public static final String UPGRADE = "Upgrade";
        public static final String USERAGENT = "User-Agent";
        public static final String VIA = "Via";
        public static final String WARNING = "Warning";
    }

    public static class Response {
        public static final String ACCEPTRANGES = "Accept-Ranges";
        public static final String AGE = "Age";
        public static final String ALLOW = "Allow";
        public static final String CACHECONTROL = "Cache-Control";
        public static final String CONNECTION = "Connection";
        public static final String CONTENTENCODING = "Content-Encoding";
        public static final String CONTENTLANGUAGE = "Content-Language";
        public static final String CONTENTLENGTH = "Content-Length";
        public static final String CONTENTLOCATION = "Content-Location";
        public static final String CONTENTMD5 = "Content-MD5";
        public static final String CONTENTRANGE = "Content-Range";
        public static final String CONTENTSECURITYPOLICY = "Content-Security-Policy";
        public static final String CONTENTTYPE = "Content-Type";
        public static final String DATE = "Date";
        public static final String ETAG = "E-Tag";
        public static final String EXPIRES = "Expires";
        public static final String LASTMODIFIED = "Last-Modified";
        public static final String LINK = "Link";
        public static final String LOCATION = "Location";
        public static final String PRAGMA = "Pragma";
        public static final String PROXYAUTHENTICATE = "Proxy-Authenticate";
        public static final String REFRESH = "Refresh";
        public static final String RETRYAFTER = "Retry-After";
        public static final String SERVER = "Server";
        public static final String SETCOOKIE = "Set-Cookie";
        public static final String TRAILER = "Trailer";
        public static final String TRANSFERENCODING = "Transfer-Encoding";
        public static final String VARY = "Vary";
        public static final String VIA = "Via";
        public static final String WARNING = "Warning";
        public static final String WWWAUTHENTICATE = "WWW-Authenticate";
    }
}