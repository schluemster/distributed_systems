package ch.ethz.inf.vs.a2.freipa.webservices.task3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpMethods;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpRequest;
import ch.ethz.inf.vs.a2.freipa.webservices.http.HttpResponse;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.pages.ActuatorPage;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.pages.MainPage;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.pages.SensorGet;
import ch.ethz.inf.vs.a2.freipa.webservices.task3.pages.WebPage;
import ch.ethz.inf.vs.a2.freipa.webservices.utils.StringMap;
import ch.ethz.inf.vs.a2.freipa.webservices.utils.Tuple;

/**
 * Created by schluemi on 21/10/16.
 */

public class RequestHandler extends BroadcastReceiver {
    private Map<Tuple<String, String>, Mapping> mappings;
    private StringMap mainNavigation;
    private StringMap subNavigation;
    private Map<Integer, Tuple<String, float[]>> sensorValues;
    private ActuatorService actuatorService;

    RequestHandler() {
        mappings = new LinkedHashMap<>();
        List<Mapping> mappingInformation = new ArrayList<>();
        mainNavigation = new StringMap();
        subNavigation = new StringMap();
        subNavigation.put("/", "Root page");
        sensorValues = new LinkedHashMap<>();

        //I could make different classes for each request but I'm really tired now
        mappingInformation.add(new Mapping("Accelerometer", "/sensor?type=1", HttpMethods.GET, SensorGet.class));
        mappingInformation.add(new Mapping("Orientation", "/sensor?type=3", HttpMethods.GET, SensorGet.class));
        mappingInformation.add(new Mapping("Gravity", "/sensor?type=9", HttpMethods.GET, SensorGet.class));
        mappingInformation.add(new Mapping("Light", "/sensor?type=5", HttpMethods.GET, SensorGet.class));
        mappingInformation.add(new Mapping("Pressure", "/sensor?type=6", HttpMethods.GET, SensorGet.class));
        mappingInformation.add(new Mapping("Vibration tool", "/actuator?actuator=vibrator", HttpMethods.GET, ActuatorPage.class));
        mappingInformation.add(new Mapping("Vibration tool", "/actuator?actuator=vibrator", HttpMethods.POST, ActuatorPage.class));
        mappingInformation.add(new Mapping("Annoying sound tool", "/actuator?actuator=music", HttpMethods.GET, ActuatorPage.class));
        mappingInformation.add(new Mapping("Annoying sound tool", "/actuator?actuator=music", HttpMethods.POST, ActuatorPage.class));
        mappingInformation.add(new Mapping("Root Page", "/", HttpMethods.GET, MainPage.class));

        for (Mapping m : mappingInformation) {
            if (m.method.equals(HttpMethods.GET) && !m.uri.equals("/")) {
                mainNavigation.put(m.uri, m.title);
            }
            mappings.put(new Tuple<>(m.uri, m.method), m);
        }
    }

    void setActuatorService(ActuatorService service)
    {
        this.actuatorService = service;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Integer type = intent.getExtras().getInt("SensorType");
        float[] values = (float[])intent.getExtras().get("SensorValues");
        String name = intent.getExtras().getString("SensorName");;
        sensorValues.put(type, new Tuple<>(name, values));
    }

    public HttpResponse handleRequest(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setStatus("200");
        response.setReason("OK");
        Tuple<String, String> mapKey = new Tuple<>(request.getRequestUri(), request.getMethod());
        try {
            if (mappings.containsKey(mapKey)) {
                Mapping m = mappings.get(mapKey);
                StringMap navigation = m.uri.equals("/") ? mainNavigation : subNavigation;
                WebPage responsePage = (WebPage) m.pageType.getConstructor(String.class, String.class, StringMap.class, HttpRequest.class, HttpResponse.class).newInstance(m.title, m.uri, navigation, request, response);
                responsePage.setDataValue(sensorValues);
                responsePage.setActuatorService(actuatorService);
                response.setBody(responsePage.generateHtml());
            } else {
                //404 Request
                response.setStatus("404");
                response.setReason("Resource not found");
                response.setBody(HtmlBuilder.ErrorPage(response.getStatus(), response.getReason(), subNavigation));
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus("500");
            response.setReason("Internal server error");
            response.setBody(HtmlBuilder.ErrorPage(response.getStatus(), response.getReason(), subNavigation));
        }
        return response;
    }

    class Mapping {
        public final String title;
        public final String uri;
        public final String method;
        public final Class pageType;

        public Mapping(String title, String uri, String method, Class pageType) {
            this.title = title;
            this.uri = uri;
            this.method = method;
            this.pageType = pageType;
        }
    }
}
