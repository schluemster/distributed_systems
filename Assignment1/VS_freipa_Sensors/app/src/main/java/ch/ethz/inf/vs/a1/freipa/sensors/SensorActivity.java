package ch.ethz.inf.vs.a1.freipa.sensors;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class SensorActivity extends AppCompatActivity implements SensorEventListener, GraphContainer {
    Intent intent = new Intent();
    private SensorManager mSensorManager;
    private Sensor mSensor;
    SensorTypesImpl mSensorType;
    float[] sensorData;
    TextView xOutput;
    TextView yOutput;
    TextView zOutput;
    TextView sensorNameOutput;
    GraphView graph;
    int sensorAmount;
    String sensorUnit;
    int maxlength = 100;

    private SortedMap<Double, float[]> allValues = new TreeMap<>();

    @Override
    public void addValues(double xIndex, float[] values) {
        allValues.put(xIndex, values);

        //removeOutOfRangeValues, might be the most recently added values ;)
        if(allValues.size() > maxlength)
        {
            allValues.remove(allValues.firstKey());
        }
    }

    @Override
    public float[][] getValues() {
        int width = allValues.size();
        float[][] returner = new float[3][width];
        int idx = 0;
        for (Double k : allValues.keySet())
        {
            float[] val = allValues.get(k);
            returner[0][idx] = val[0];
            returner[1][idx] = val[1];
            returner[2][idx] = val[2];
            idx++;
        }
        return returner;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sensor_activity);
        graph = (GraphView) findViewById(R.id.graph);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxX(1);
        graph.getViewport().setMaxX(100);
        intent = getIntent();
        String sensorType = intent.getStringExtra(MainActivity.sensorType);
        String sensorName = intent.getStringExtra(MainActivity.sensorName);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(MainActivity.sensorHash.get(sensorName));
        mSensorType = new SensorTypesImpl();

        sensorAmount = mSensorType.getNumberValues(MainActivity.sensorHash.get(sensorName));
        sensorUnit = mSensorType.getUnitString(MainActivity.sensorHash.get(sensorName));
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return super.formatLabel(value, isValueX);
                } else {
                    return super.formatLabel(value, isValueX) + sensorUnit;
                }
            }
        });
        if (sensorAmount != -1){
            sensorData = new float[sensorAmount];
        }
        xOutput = (TextView) findViewById(R.id.xView);
        yOutput = (TextView) findViewById(R.id.yView);
        zOutput = (TextView) findViewById(R.id.zView);
        sensorNameOutput = (TextView) findViewById(R.id.sensorView);
        sensorNameOutput.setText(sensorName);
        //outputText.setText(String.valueOf(x)+" "+String.valueOf(y)+" "+String.valueOf(z));
    }

    public GraphContainer getGraphContainer() {return this;}

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        float[][] graphPoints = new float[3][100];
        if(sensorAmount == -1) {
            xOutput.setText("Unfortunately, there is no information to display about this sensor.");
        }else{
            // Graph part
            sensorData = event.values.clone();
            this.addValues(System.currentTimeMillis(), new float[]{sensorData[0], sensorData[1], sensorData[2]});
            float[][] values = this.getValues();
            graph.removeAllSeries();
            for (int i=0; i<3; i++) {
                List<DataPoint> dataPoints = new ArrayList<>();
                int idx = 0;
                for (Float f : values[i]) {
                    dataPoints.add(new DataPoint(idx, f));
                    idx++;
                }
                DataPoint[] dp = new DataPoint[dataPoints.size()];
                graph.addSeries(new LineGraphSeries<>(dataPoints.toArray(dp)));
            }
            // Graph part end
            xOutput.setText("x: "+String.valueOf(sensorData[0])+ " "+ sensorUnit);
            yOutput.setText("y: "+String.valueOf(sensorData[1])+ " "+ sensorUnit);
            zOutput.setText("z: "+String.valueOf(sensorData[2])+ " "+ sensorUnit);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do not do anything
    }
}
