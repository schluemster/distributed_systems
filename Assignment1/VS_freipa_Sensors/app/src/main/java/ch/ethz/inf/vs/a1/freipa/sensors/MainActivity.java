package ch.ethz.inf.vs.a1.freipa.sensors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.hardware.SensorManager;
import android.hardware.Sensor;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Hashtable;



public class MainActivity extends AppCompatActivity {
    public static final String sensorType = "SENSORTYPE";
    public static final String sensorName = "SENSORNAME";
    public static Hashtable<String, Integer> sensorHash = new Hashtable<String, Integer>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //final Hashtable<String, Integer> sensorHash = new Hashtable<String, Integer>();
        //final List<Entry<String, Integer>> sensorList =new ArrayList<Entry<String, Integer>>();
        final List<String> sensorNames = new ArrayList<String>();

        SensorManager sMgr;
        sMgr = (SensorManager)this.getSystemService(SENSOR_SERVICE);
        List<Sensor> list = sMgr.getSensorList(Sensor.TYPE_ALL);
        for(Sensor sensor: list){
            Entry entry = new AbstractMap.SimpleEntry<>(sensor.getName(), sensor.getType());
            sensorHash.put(sensor.getName(), sensor.getType());
            //sensorList.add(entry);
            sensorNames.add(sensor.getName());

        }

        ArrayAdapter<String> myAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sensorNames);

        ListView myList= (ListView) findViewById(R.id.listView);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = ((TextView)view).getText().toString();
                Intent intent = new Intent(MainActivity.this, SensorActivity.class);
                //Toast.makeText(getBaseContext(), item+sensorHash.get(item).toString(), Toast.LENGTH_LONG).show();
                intent.putExtra(sensorType, sensorHash.get(item).toString());
                intent.putExtra(sensorName, item);
                startActivity(intent);
            }
        });


        myList.setAdapter(myAdapter);



    }


}
