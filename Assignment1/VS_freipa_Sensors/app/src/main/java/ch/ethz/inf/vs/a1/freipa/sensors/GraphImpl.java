package ch.ethz.inf.vs.a1.freipa.sensors;

/**
 * Created by Jonathan on 13.10.2016.
 */

public class GraphImpl implements GraphContainer {

    float[][] graphLine = new float[3][100];
    
    @Override
    public void addValues(double xIndex, float[] values) {
        for(int i=0;i<3;i++){
            graphLine[i][(int) xIndex]=values[i];
        }
    }

    @Override
    public float[][] getValues() {
        return graphLine;
    }
}
