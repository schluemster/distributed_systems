package ch.ethz.inf.vs.a1.freipa.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Semaphore;


public class DeviceDetailsActivity extends AppCompatActivity implements GraphContainer {
    BluetoothDevice bluetoothDevice;
    private BluetoothGatt bluetoothGatt;
    private Handler viewUpdateHandler;
    Semaphore writeAvailable;
    private float latestHum = 0;
    private float latestTemp = 0;
    private GraphView graphView;
    private int maxlength = 100;
    private SortedMap<Double, float[]> allValues = new TreeMap<>();

    private BluetoothGattCallback bluetoothGattCallback;

    @Override
    public void addValues(double xIndex, float[] values) {
        allValues.put(xIndex, values);

        //removeOutOfRangeValues, might be the most recently added values ;)
        if (allValues.size() > maxlength) {
            allValues.remove(allValues.firstKey());
        }
    }

    @Override
    public float[][] getValues() {
        int width = allValues.size();
        float[][] returner = new float[2][width];
        int idx = 0;
        for (Double k : allValues.keySet()) {
            float[] val = allValues.get(k);
            returner[0][idx] = val[0];
            returner[1][idx] = val[1];
            idx++;
        }
        return returner;
    }

    private void connect() {
        Button connectionButton = (Button) findViewById(R.id.button_connect_ble);
        connectionButton.setEnabled(false);
        connectionButton.setText(R.string.connecting);
        bluetoothGatt = bluetoothDevice.connectGatt(this, false, bluetoothGattCallback);
    }

    private void disconnect() {
        if (bluetoothGatt != null) {
            bluetoothGatt.disconnect();
        }
    }

    private float convertRawValue(byte[] raw) {
        ByteBuffer wrapper = ByteBuffer.wrap(raw).order(ByteOrder.LITTLE_ENDIAN);
        return wrapper.getFloat();
    }

    private void updateValues(BluetoothGattCharacteristic characteristic) {
        Log.d("UPDATE_VALUES", String.valueOf(convertRawValue(characteristic.getValue())));
        TextView textView = null;
        String suffix = "";
        float newValue = convertRawValue(characteristic.getValue());

        if (characteristic.getUuid().equals(SensirionSHT31UUIDS.UUID_HUMIDITY_CHARACTERISTIC)) {
            textView = (TextView) findViewById(R.id.text_humidity);
            suffix = "%";
            latestHum = newValue;
        }
        if (characteristic.getUuid().equals(SensirionSHT31UUIDS.UUID_TEMPERATURE_CHARACTERISTIC)) {
            textView = (TextView) findViewById(R.id.text_temperature);
            suffix = "°C";
            latestTemp = newValue;
        }
        if (textView != null) {
            textView.setText(newValue + suffix);
        }
        this.addValues(System.currentTimeMillis(), new float[]{latestHum, latestTemp});
        //GraphView graph = (GraphView) findViewById(R.id.graph);
        float[][] values = this.getValues();

        List<DataPoint> dataPoints0 = new ArrayList<>();
        int idx = 0;
        for (Float f : values[0]) {
            dataPoints0.add(new DataPoint(idx, f));
            idx++;
        }
        DataPoint[] dp0 = new DataPoint[dataPoints0.size()];
        LineGraphSeries<DataPoint> dataPointSeries0 = new LineGraphSeries<>(dataPoints0.toArray(dp0));

        List<DataPoint> dataPoints1 = new ArrayList<>();
        idx = 0;
        for (Float f : values[1]) {
            dataPoints1.add(new DataPoint(idx, f));
            idx++;
        }
        DataPoint[] dp1 = new DataPoint[dataPoints1.size()];
        LineGraphSeries<DataPoint> dataPointSeries1 = new LineGraphSeries<>(dataPoints1.toArray(dp1));
        graphView.removeAllSeries();
        graphView.addSeries(dataPointSeries0);
        graphView.addSeries(dataPointSeries1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        writeAvailable = new Semaphore(1);
        Button connectionButton = (Button) findViewById(R.id.button_connect_ble);
        connectionButton.setOnClickListener(null);
        if (bluetoothGatt == null || bluetoothGatt.getConnectedDevices().isEmpty()) {
            connectionButton.setText(R.string.connect);
            connectionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    connect();
                }
            });
        }
        //Let's just start over in a clean state
        else {
            connectionButton.setText(R.string.disconnect);
            connectionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disconnect();
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //let's disconnect again, just to be sure ;)
        disconnect();
        viewUpdateHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewUpdateHandler = new Handler();
        setContentView(R.layout.activity_device_details);
        Intent intent = getIntent();
        bluetoothDevice = intent.getExtras().getParcelable("device");
        TextView textView = (TextView) findViewById(R.id.text_remote_device_id);
        textView.setText(bluetoothDevice.getAddress());
        graphView = (GraphView) findViewById(R.id.graph);
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setMaxX(1);
        graphView.getViewport().setMaxX(100);
        bluetoothGattCallback = new BluetoothGattCallback() {

            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                super.onConnectionStateChange(gatt, status, newState);
                Log.i("onConnectionStateChange", "Status: " + status);
                switch (newState) {
                    case BluetoothProfile.STATE_CONNECTED:
                        //queuedTasks = new LinkedList<>();
                        Log.i("gattCallback", "STATE_CONNECTED");
                        viewUpdateHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Button connectionButton = (Button) findViewById(R.id.button_connect_ble);
                                connectionButton.setEnabled(true);
                                connectionButton.setText(R.string.disconnect);
                                connectionButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        disconnect();
                                    }
                                });
                            }
                        }, 0);
                        gatt.discoverServices();
                        break;
                    case BluetoothProfile.STATE_DISCONNECTED:
                        Log.e("gattCallback", "STATE_DISCONNECTED");
                        viewUpdateHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Button connectionButton = (Button) findViewById(R.id.button_connect_ble);
                                connectionButton.setText(R.string.connect);
                                connectionButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        connect();
                                    }
                                });
                            }
                        }, 0);
                        break;
                    default:
                        Log.e("gattCallback", "STATE_OTHER");
                }
            }

            @Override
            public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
                super.onServicesDiscovered(gatt, status);
                List<BluetoothGattService> services = gatt.getServices();
                Log.i("DONE_DISCOVER_SERVICES", services.toString());

                //add read permission to humidity service
                BluetoothGattService humidityService = gatt.getService(SensirionSHT31UUIDS.UUID_HUMIDITY_SERVICE);
                final BluetoothGattCharacteristic humidityCharacteristic = humidityService.getCharacteristic(SensirionSHT31UUIDS.UUID_HUMIDITY_CHARACTERISTIC);
                final BluetoothGattCharacteristic humidityCharacteristicNew = new BluetoothGattCharacteristic(SensirionSHT31UUIDS.UUID_HUMIDITY_CHARACTERISTIC, humidityCharacteristic.getProperties(), humidityCharacteristic.getPermissions() | BluetoothGattCharacteristic.PERMISSION_READ);

                //enable notifications for humidity service
                final BluetoothGattDescriptor humidityUpdateDescriptor = new BluetoothGattDescriptor(SensirionSHT31UUIDS.NOTIFICATION_DESCRIPTOR_UUID, 1);
                humidityUpdateDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                humidityCharacteristicNew.addDescriptor(humidityUpdateDescriptor);
                humidityService.addCharacteristic(humidityCharacteristicNew);
                gatt.setCharacteristicNotification(humidityCharacteristicNew, true);

                //add read permission to temperature service
                BluetoothGattService temperatureService = gatt.getService(SensirionSHT31UUIDS.UUID_TEMPERATURE_SERVICE);
                final BluetoothGattCharacteristic temperatureCharacteristic = temperatureService.getCharacteristic(SensirionSHT31UUIDS.UUID_TEMPERATURE_CHARACTERISTIC);
                final BluetoothGattCharacteristic temperatureCharacteristicNew = new BluetoothGattCharacteristic(SensirionSHT31UUIDS.UUID_TEMPERATURE_CHARACTERISTIC, temperatureCharacteristic.getProperties(), temperatureCharacteristic.getPermissions() | BluetoothGattCharacteristic.PERMISSION_READ);

                //enable notifications for temperature service
                final BluetoothGattDescriptor temperatureUpdateDescriptor = new BluetoothGattDescriptor(SensirionSHT31UUIDS.NOTIFICATION_DESCRIPTOR_UUID, 1);
                temperatureUpdateDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                temperatureCharacteristicNew.addDescriptor(temperatureUpdateDescriptor);
                temperatureService.addCharacteristic(temperatureCharacteristicNew);
                gatt.setCharacteristicNotification(temperatureCharacteristicNew, true);


                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            writeAvailable.acquire(1);
                            Log.d("PERMISSIONS_CHANGE", "TEMPERATURE CHAR");
                            gatt.writeCharacteristic(temperatureCharacteristicNew);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();*/

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            writeAvailable.acquire(1);
                            Log.d("PERMISSIONS_CHANGE", "TEMPERATURE DESC");
                            gatt.writeDescriptor(temperatureUpdateDescriptor);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            writeAvailable.acquire(1);
                            Log.d("PERMISSIONS_CHANGE", "HUMIDITY CHAR");
                            gatt.writeCharacteristic(humidityCharacteristicNew);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();*/

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            writeAvailable.acquire(1);
                            Log.d("PERMISSIONS_CHANGE", "HUMIDITY DESC");
                            gatt.writeDescriptor(humidityUpdateDescriptor);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                Log.d("PERMISSIONS_CHANGE", "5");
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);
                Log.d("CHARA_CHANGED", "SHIT JUST GOT REAL");
                viewUpdateHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateValues(characteristic);
                    }
                }, 0);
            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onCharacteristicWrite(gatt, characteristic, status);
                Log.d("CHAR TASK DONE", String.valueOf(status));
                writeAvailable.release(1);
            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                super.onDescriptorWrite(gatt, descriptor, status);
                Log.d("DESC TASK DONE", String.valueOf(status));
                writeAvailable.release(1);
            }
        };
    }
}
