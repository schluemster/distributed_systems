package ch.ethz.inf.vs.a1.freipa.ble;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by schluemi on 12/10/16.
 */
/*
public class Grapher implements GraphContainer {

    private int maxlength = 100;
    private SortedMap<Double, float[]> allValues;

    public Grapher()
    {
        allValues = new TreeMap<>();
    }

    @Override
    public void addValues(double xIndex, float[] values) {
        allValues.put(xIndex, values);

        //removeOutOfRangeValues, might be the most recently added values ;)
        if(allValues.size() > maxlength)
        {
            allValues.remove(allValues.firstKey());
        }
    }

    @Override
    public float[][] getValues() {
        int width = allValues.size();
        float[][] returner = new float[2][width];
        int idx = 0;
        for (Double k : allValues.keySet())
        {
            float[] val = allValues.get(k);
            returner[0][idx] = val[0];
            returner[1][idx] = val[1];
            idx++;
        }
        return returner;
    }
}
*/