package ch.ethz.inf.vs.a1.freipa.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.vs.a1.freipa.ble.SensirionSHT31UUIDS;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter mBluetoothAdapter;
    private static final long SCAN_PERIOD = 10000;
    private Handler mHandler;
    private BluetoothLeScanner mLEScanner;
    private ScanCallback scanCallback;
    private List<BluetoothDevice> bluetoothDevices;
    List<ScanFilter> filters;
    ScanSettings settings;
    Timer timer;

    private final static int REQUEST_ENABLE_BT = 1;

    public void startScan(View view)
    {
        bluetoothDevices = new ArrayList<>();
        listBLEDevices();
        Button button = (Button) view;
        button.setEnabled(false);
        
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                    stopScan();
                }
        }, SCAN_PERIOD);
        mLEScanner.startScan(filters, settings, scanCallback);
        timer = new Timer(SCAN_PERIOD, 1000, (TextView) findViewById(R.id.text_scan_timer), "Scan stops in ", "s");
        timer.run();
    }

    public void stopScan()
    {
        if (timer != null) {
            timer.stop();
        }
        Button button = (Button) findViewById(R.id.button_scan_start);
        button.setEnabled(true);

        mLEScanner.stopScan(scanCallback);
        mLEScanner.flushPendingScanResults(scanCallback);
    }

    private void listBLEDevices()
    {
        List<String> deviceAddresses = new ArrayList<String>();
        for(BluetoothDevice device : bluetoothDevices)
        {
            Log.d("listBLEDevices", device.getAddress() + ", " + device.getName() + ", " + device.getBluetoothClass() + ", " + device.getType());
            deviceAddresses.add(device.getAddress());

        }
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, deviceAddresses);
        ListView listView = (ListView) findViewById(R.id.list_ble_devices);
        listView.setAdapter(itemsAdapter);
    }

    private void getDeviceDetails(BluetoothDevice device)
    {
        stopScan();
        mHandler.removeCallbacksAndMessages(null);
        Intent intent = new Intent(this, DeviceDetailsActivity.class);
        intent.putExtra("device", device);
        startActivity(intent);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
        stopScan();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        checkAndInitializeBluetoothAdapter();
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    private void addDevice(BluetoothDevice device)
    {
        if (!bluetoothDevices.contains(device)) {
            bluetoothDevices.add(device);
        }
        listBLEDevices();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAndInitializeBluetoothAdapter();
        setContentView(R.layout.activity_main);
        mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
        mHandler = new Handler();
        filters = new ArrayList<>();
        filters.add(new ScanFilter.Builder().setDeviceName("Smart Humigadget").build());
        settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();

        scanCallback = new ScanCallback(){
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                addDevice(result.getDevice());
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                super.onBatchScanResults(results);
                for (ScanResult result : results)
                {
                    addDevice(result.getDevice());
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
                Log.e("BLE_SCAN", "Scan failed with error code " + errorCode);
            }
        };

        final ListView listView = (ListView) findViewById(R.id.list_ble_devices);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                getDeviceDetails(bluetoothDevices.get(myItemInt));
            }
        });
    }

    private void checkAndInitializeBluetoothAdapter()
    {
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
