package ch.ethz.inf.vs.a1.freipa.ble;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

/**
 * Created by schluemi on 07/10/16.
 */

public class Timer {
    long duration;
    long interval;
    TextView textView;
    String prefix;
    String suffix;

    Handler tHandler;
    Runnable tRunnable;

    public Timer(long duration, long interval, TextView textView, String prefix, String suffix)
    {
        this.duration = duration;
        this.interval = interval;
        this.textView = textView;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public void run()
    {
        displayTime();
        tHandler = new Handler();
        tRunnable = new Runnable() {
            @Override
            public void run() {
                if (duration - interval >= 0)
                {
                    duration -= interval;
                    tHandler.postDelayed(this, interval);
                }
                else
                {
                    duration = 0;
                }
                displayTime();
            }
        };
        tHandler.postDelayed(tRunnable, interval);
    }

    public void pause()
    {
        tHandler.removeCallbacksAndMessages(null);
    }

    public void stop()
    {
        pause();
        this.textView.setText("");
    }

    private void displayTime()
    {
        this.textView.setText(prefix + (((double)duration)/1000) + suffix);
    }

}
