package ch.ethz.inf.vs.a1.freipa.antitheft;

public interface AlarmCallback {
    void onDelayStarted();
}
