package ch.ethz.inf.vs.a1.freipa.antitheft.movement_detector;

import ch.ethz.inf.vs.a1.freipa.antitheft.AlarmCallback;

public class SingleSpikeMovementDetector extends AbstractMovementDetector {

    public SingleSpikeMovementDetector(AlarmCallback callback, int sensitivity) {
        super(callback, sensitivity);
    }

    @Override
    public boolean doAlarmLogic(float[] values) {
        // TODO, insert your logic here

        for (float element : values) {
            if (element >= this.sensitivity) {
                return true;
            }
        }
        return false;
    }
}
