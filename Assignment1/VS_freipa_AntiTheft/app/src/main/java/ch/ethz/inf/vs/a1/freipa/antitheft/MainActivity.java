package ch.ethz.inf.vs.a1.freipa.antitheft;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    protected boolean isLocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.isLocked = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.settings:
                if (isLocked) {
                    Toast.makeText(this, "Stop alarm to access settings", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(this, SettingsActivity.class));
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ToggleButton tb = (ToggleButton) findViewById(R.id.toggle_button);
        if (isMyServiceRunning(AntiTheftService.class)) {
            tb.setChecked(true);
        } else {
            tb.setChecked(false);
        }
    }

    public void lockDevice() {
        this.isLocked = true;
    }

    public void unlockDevice() {
        this.isLocked = false;
    }

    public void startService() {
        startService(new Intent(this, AntiTheftService.class));
    }

    public void stopService() {
        stopService(new Intent(this, AntiTheftService.class));
    }

    public void onClickToggle(View view) {
        ToggleButton tb = (ToggleButton) view;
        if (tb.isChecked()) {
            tb.setText(R.string.btn_text_checked);
            startService();
            lockDevice();
        } else {
            tb.setText(R.string.btn_text_unchecked);
            stopService();
            unlockDevice();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
