package ch.ethz.inf.vs.a1.freipa.antitheft.movement_detector;

import ch.ethz.inf.vs.a1.freipa.antitheft.AlarmCallback;

public class SpikeMovementDetector extends AbstractMovementDetector {

    public SpikeMovementDetector(AlarmCallback callback, int sensitivity) {
        super(callback, sensitivity);
    }

    @Override
    public boolean doAlarmLogic(float[] values) {
        // TODO, insert your logic here

        float sum = 0;
        for (float element : values) {
            sum += Math.abs(element);
        }

        if ((int) sum >= this.sensitivity) {
            return true;
        }
        return false;
    }
}
