package ch.ethz.inf.vs.a1.freipa.antitheft;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import ch.ethz.inf.vs.a1.freipa.antitheft.movement_detector.AbstractMovementDetector;
import ch.ethz.inf.vs.a1.freipa.antitheft.movement_detector.SingleSpikeMovementDetector;
import ch.ethz.inf.vs.a1.freipa.antitheft.movement_detector.SpikeMovementDetector;


public class AntiTheftService extends Service implements AlarmCallback {
    protected int notificationId;
    protected MediaPlayer alarmPlayer;
    private AbstractMovementDetector movementDetector;
    private SensorManager mSensorManager;
    private int delay;


    public AntiTheftService() {
        super();
        this.notificationId = 37;
    }

    @Override
    public void onCreate() {
        // The service is being created
        Log.d("AntiTheftService", "Service created!");
        alarmPlayer = MediaPlayer.create(this, R.raw.alarm_sound);
        alarmPlayer.setLooping(true);

        // Get preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Delay
        this.delay = Integer.valueOf(preferences.getString("delay", getResources().getString(R.string.pref_delay_default)));
//       Log.d("delay", String.valueOf(this.delay));

        // Sensitivity
        int sensitivity = Integer.valueOf(preferences.getString("sensitivity", getResources().getString(R.string.pref_sensitivity_default)));
//       Log.d("sensitivity", String.valueOf(sensitivity));

        // Movement detector type
        String detector = preferences.getString("detector", getResources().getString(R.string.pref_detector_default));
        if (detector.equals(getResources().getString(R.string.pref_detector_default))) {
//           Log.d("detector", "using sum detector");
            movementDetector = new SpikeMovementDetector(this, sensitivity);
        } else {
//           Log.d("detector", "using single detector");
            movementDetector = new SingleSpikeMovementDetector(this, sensitivity);
        }

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mSensorManager.registerListener(movementDetector, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // The service is starting, due to a call to startService()
        Log.d("AntiTheftService", "Service starting!");
        return START_STICKY;
    }

    @Override
    public void onRebind(Intent intent) {
        // A client is binding to the service with bindService(),
        // after onUnbind() has already been called
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        Log.d("AntiTheftService", "Service stopping!");
        cancelNotification();
        stopAlarm();
        // mSensorManager.unregisterListener(movementDetector);
    }

    private void sendNotification() {
        NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setContentTitle(getResources().getString(R.string.alarm_title))
                .setContentText(getResources().getString(R.string.alarm_text))
                .setOngoing(true);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
            stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(notificationId, mBuilder.build());
    }

    private void cancelNotification() {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.cancel(notificationId);
    }

    private void startAlarm() {
        mSensorManager.unregisterListener(movementDetector);
        this.alarmPlayer.start();
    }

    private void stopAlarm() {
        this.alarmPlayer.stop();
    }

    public void onDelayStarted() {
        SystemClock.sleep((long) (delay*1000));
        sendNotification();
        startAlarm();
    }
}
