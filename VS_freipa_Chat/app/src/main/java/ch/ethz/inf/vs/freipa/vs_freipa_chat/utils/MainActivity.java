package ch.ethz.inf.vs.freipa.vs_freipa_chat.utils;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import org.json.JSONException;

import java.util.UUID;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.R;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.Message;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.MessageTypes;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.task3.ChatActivity;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient.NotificationHandler;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient.NetworkConsts;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient.SettingsActivity;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient.UdpClient;

public class MainActivity extends AppCompatActivity implements MessageReceiver {

    NotificationHandler notificationHandler;
    UdpClient udpClient = null;

    private String registerUuid;
    private int registerCounter;
    private String currentUsername;

    public static String currentIP;
    public static int currentPort = NetworkConsts.UDP_PORT;
    public static String IPADDRESS = "IPADRESS";
    public static String PORT = "PORT";

    private int registerMessageId;

    Intent settingsIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        settingsIntent = getIntent();
        if(!settingsIntent.equals(null)){
            currentIP = "192.168.";
            currentPort = settingsIntent.getIntExtra(SettingsActivity.PORT, 4446);
        }
        TextView debugView = (TextView) findViewById(R.id.debug);
        debugView.setText("Server IP address: "+currentIP + " | Server port: "+currentPort);
    }

    @Override
    public void onResume() {
        super.onResume();
        notificationHandler = new NotificationHandler(this);
        if (udpClient == null || udpClient.isHalted())
        {
            udpClient = new UdpClient();
        }
        settingsIntent = getIntent();
        if(!settingsIntent.equals(null)){
            currentIP = settingsIntent.getStringExtra(SettingsActivity.IPADDRESS);
            currentPort = settingsIntent.getIntExtra(SettingsActivity.PORT, 4446);
        }
        TextView debugView = (TextView) findViewById(R.id.debug);
        debugView.setText(String.format("Server IP address: "+currentIP + "%nServer port: "+currentPort));
    }

    @Override
    public void onPause() {
        super.onPause();
        udpClient.haltAndCleanUp();
    }

    public void sendRegistration(View view){
        final EditText username =  (EditText) findViewById(R.id.usernameeditText);
        currentUsername = username.getText().toString();
        registerUuid = UUID.randomUUID().toString();
        Message regPacket = new Message(username.getText().toString(), MessageTypes.REGISTER, registerUuid, null, "{}");
        registerMessageId = udpClient.postMessage(regPacket, this.notificationHandler, currentIP, currentPort);
        Log.d("MAINACTIVITY", "this is my message id: " + registerMessageId + ", yay ");
    }

    public void goToSettings(View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra(IPADDRESS, currentIP);
        intent.putExtra(PORT, currentPort);
        startActivity(intent);
    }

    @Override
    public void onPacketSent(int requestId) {
        Log.d("MESSAGERECEIVER", "message with id " + requestId + " has been sent");
    }

    @Override
    public void onResultAvailable(int requestId) {
        Message regPacket = null;
        if(requestId == registerMessageId){
            try {
                Message received = Message.parse(udpClient.getResult(requestId));
                if (received.getMessageType().equals(MessageTypes.ACK_MESSAGE)){
                    Intent intent = new Intent(this, ChatActivity.class);
                    intent.putExtra("username", currentUsername);
                    intent.putExtra("uuid", registerUuid);
                    intent.putExtra("address", currentIP);
                    intent.putExtra("port", currentPort);
                    startActivity(intent);
                }else if(received.getMessageType().equals(MessageTypes.ERROR_MESSAGE)){
                    if(registerCounter < 5) {
                        regPacket = new Message(currentUsername, MessageTypes.REGISTER, null, "{}");
                        registerMessageId = udpClient.postMessage(regPacket, this.notificationHandler, currentIP, currentPort);
                        registerCounter++;
                    }else{
                        //TODO notification error message
                        new AlertDialog.Builder(this)
                                .setTitle("Register Error")
                                .setMessage("Your registration was unsuccessful. Please try another username.")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        registerCounter = 0;
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        Log.d("MESSAGERECEIVER", "message with id " + requestId + " has some nice results waiting for you");
    }

    @Override
    public void onPacketNotSent(int requestId) {
        Log.d("MESSAGERECEIVER", "couldn't send message with id " + requestId);
    }

    @Override
    public void onReceiveTimeOut(int requestId) {
        if(registerCounter < 5) {
            Message regPacket = new Message(currentUsername, MessageTypes.REGISTER, null, "{}");
            registerMessageId = udpClient.postMessage(regPacket, this.notificationHandler, currentIP, currentPort);
            registerCounter++;
        }else{
            new AlertDialog.Builder(this)
                    .setTitle("Server Error!")
                    .setMessage("Server not responding")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            registerCounter = 0;
        }
        Log.d("MESSAGERECEIVER", "no longer waiting for a response to packet number " + requestId);
    }

    @Override
    public void onReceiveError(int requestId) {
        Log.d("MESSAGERECEIVER", "packet " + requestId + " is really fucked up somehow. you should send the request again!");
    }
}
