package ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.R;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.utils.MainActivity;

public class SettingsActivity extends AppCompatActivity {
    public static String IPADDRESS = "IPADRESS";
    public static String PORT = "PORT";
    String ip= "192.168.";
    int portnumber;
    Intent mainIntent;
    EditText et;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        mainIntent = getIntent();
        et = (EditText) findViewById(R.id.IPeditText);
        et.setText(ip);
        //et = (EditText) findViewById(R.id.porteditText);
        //et.setText(MainActivity.currentPort);
    }

    public void confirmSettings(View view){
        Intent intent = new Intent(this, MainActivity.class);
        EditText editText = (EditText) findViewById(R.id.IPeditText);
        ip = editText.getText().toString();
        editText = (EditText) findViewById(R.id.porteditText);
        portnumber = Integer.valueOf(editText.getText().toString());
        intent.putExtra(IPADDRESS, ip);
        intent.putExtra(PORT, portnumber);
        startActivity(intent);
    }
}
