package ch.ethz.inf.vs.freipa.vs_freipa_chat.message;

import java.util.Comparator;
import java.util.Date;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.clock.VectorClockComparator;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.message.Message;
import ch.ethz.inf.vs.freipa.vs_freipa_chat.task2.VectorClock;

/**
 * Message comparator class. Use with PriorityQueue.
 */
public class MessageComparator implements Comparator<Message> {

    static VectorClockComparator comparator = new VectorClockComparator();

    @Override
    public int compare(Message lhs, Message rhs) {
        VectorClock clock1 = new VectorClock();
        clock1.setClockFromString(lhs.getTimestamp());
        VectorClock clock2 = new VectorClock();
        clock2.setClockFromString(rhs.getTimestamp());
        return comparator.compare(clock1, clock2);
    }

}
