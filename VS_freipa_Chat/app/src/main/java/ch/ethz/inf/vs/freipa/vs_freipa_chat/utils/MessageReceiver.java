package ch.ethz.inf.vs.freipa.vs_freipa_chat.utils;

import android.os.Message;

/**
 * Created by schluemi on 03/11/16.
 */

public interface MessageReceiver {

    void onPacketSent(int requestId);
    void onResultAvailable(int requestId);
    void onPacketNotSent(int requestId);
    void onReceiveTimeOut(int requestId);
    void onReceiveError(int requestId);
}
