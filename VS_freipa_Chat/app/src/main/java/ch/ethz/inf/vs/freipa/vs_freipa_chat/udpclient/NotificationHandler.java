package ch.ethz.inf.vs.freipa.vs_freipa_chat.udpclient;

import android.os.Handler;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.utils.MessageReceiver;

/**
 * Created by schluemi on 03/11/16.
 */

public class NotificationHandler extends Handler {

    public static final int PACKET_SENT = 0;
    public static final int RESULT_AVAILABLE = 1;
    public static final int PACKET_NOT_SENT = 2;
    public static final int RECEIVE_TIMEOUT = 3;
    public static final int RECEIVE_ERROR = 4;

    private MessageReceiver messageReceiver;

    private NotificationHandler(){

    };

    public NotificationHandler(MessageReceiver messageReceiver)
    {
        this.messageReceiver = messageReceiver;
    }

    @Override
    public void handleMessage(android.os.Message msg) {

        switch (msg.what) {
            case PACKET_SENT:
                messageReceiver.onPacketSent((Integer)msg.obj);
                break;
            case RESULT_AVAILABLE:
                messageReceiver.onResultAvailable((Integer)msg.obj);
                break;
            case PACKET_NOT_SENT:
                messageReceiver.onPacketNotSent((Integer)msg.obj);
                break;
            case RECEIVE_TIMEOUT:
                messageReceiver.onReceiveTimeOut((Integer)msg.obj);
                break;
            case RECEIVE_ERROR:
                messageReceiver.onReceiveError((Integer)msg.obj);
                break;
            default:
                super.handleMessage(msg);
        }
    }
}
