package ch.ethz.inf.vs.freipa.vs_freipa_chat.message;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Jonathan on 27.10.2016.
 */

public class Message {

    private String username;
    private String messagetype;
    private String uuid;
    private JSONObject body;
    private String timestamp;

    public  Message(String userName, String messageType, JSONObject body, String timestamp){
        this(userName, messageType, UUID.randomUUID().toString(), body, timestamp);
    }

    public Message(String userName, String messageType, String uuid, JSONObject body, String timestamp){
        this.username = userName;
        this.messagetype = messageType;
        this.uuid = uuid;
        this.body = body == null ? new JSONObject() : body;
        this.timestamp = timestamp;
    }

    public String getUsername(){
        return username;
    }

    public String getUuid(){
        return uuid;
    }

    public JSONObject getBody(){
        return body;
    }

    public static Message parse(JSONObject jsonObject) throws JSONException {
        JSONObject header = jsonObject.getJSONObject("header");
        String userName = header.getString("username");
        String uuid = header.getString("uuid");
        String timestamp = header.getString("timestamp");
        String type = header.getString("type");
        JSONObject body = jsonObject.getJSONObject("body");
        return new Message(userName, type, uuid, body, timestamp);
    }

    public static Message parse(String stringMessage) throws JSONException {
        JSONObject jsonObject = new JSONObject(stringMessage);
        return parse(jsonObject);
    }

    public String getMessageType(){
        return messagetype;
    }
    public String getTimestamp(){
        return timestamp;
    }
    public JSONObject createJSONMessage(){
        JSONObject result = new JSONObject();
        JSONObject header = new JSONObject();

        try {
            header.put("username", this.getUsername());
            header.put("uuid", this.getUuid());
            header.put("timestamp", this.getTimestamp());
            header.put("type", this.getMessageType());
            result.put("header", header);
            result.put("body", body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

}
