package ch.ethz.inf.vs.freipa.vs_freipa_chat.task2;

import ch.ethz.inf.vs.freipa.vs_freipa_chat.clock.Clock;

/**
 * Created by ary on 28/10/16.
 */

public class LamportClock implements Clock {
    private int time;

    @Override
    public void update(Clock other) {
        int timeBefore = getTime();
        setClock(other);
        int timeAfter = getTime();
        setTime(Math.max(timeBefore, timeAfter));
    }

    @Override
    public void setClock(Clock other) {
        setClockFromString(other.toString());
    }

    @Override
    public void tick(Integer pid) {
        setTime(getTime() + 1);
    }

    @Override
    public boolean happenedBefore(Clock other) {
        int currentClockTime = getTime();
        // In order to get other clock's time.
        setClock(other);
        int otherClockTime = getTime();

        boolean happenedBefore;
        if (otherClockTime <= currentClockTime) {
            happenedBefore = false;
        } else {
            happenedBefore = true;
        }

        // Return clock to original time.
        setTime(currentClockTime);
        return happenedBefore;
    }

    @Override
    public String toString() {
        return String.valueOf(getTime());
    }

    @Override
    public void setClockFromString(String clock) {
        try {
            setTime(Integer.parseInt(clock));
        } catch (NumberFormatException e) {
            return;
        }
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTime() {
        return this.time;
    }
}
